import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')
const showMessage = function(mag:string){
  text.value = mag
  snackbar.value = true
}
  return {snackbar,text,showMessage  }
})
